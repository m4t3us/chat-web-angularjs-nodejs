var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(express.static("client"));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


var messages = [
	{date: new Date(), nickname: "Ana", message: "Oi, quer tc?"},
	{date: new Date(), nickname: "Mãe Solteira", message: "Mãe solteira quer..."},
	{date: new Date(), nickname: "João", message: "Eu quero!"}
];

app.get("/messages", function (req, res) {
	res.json(messages);
});

app.post("/messages", function (req, res) {
	var message = req.body;
	messages.push(message);
	res.json(message);
});

app.listen(3000, ()=>console.log("Porta 3000"));